<?php

define("__DRUPAL_BASE_DIR", dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR);

/**
 * Create & test the filepath.
 * @param   string  the input path
 * @return  string  the fixed full path to the file, or false
 */
function __fr_file_create_path($fn) {
  global $config;
  $basedir = __DRUPAL_BASE_DIR.$config["filereq_fspath"].DIRECTORY_SEPARATOR;
  $fn = realpath($basedir.$fn);
  if (strncmp($basedir, $fn, strlen($basedir)) != 0) return false; // security exploit
  return $fn;
}

if (file_exists("./throttle.config.php")) {
  require_once("downloadhandler.php");
  require("throttle.config.php");

  $config["filename"] = __fr_file_create_path($_GET['file']);
  if (!__fr_can_download_file($config)) define("__FILEREQ_LEECH", $_GET['file']);
  if ($config["filename"] && is_readable($config["filename"]) && !defined("__FILEREQ_LEECH")) {
    // make sure no caching headers are set
    header("Cache-Control:");
    header("Pragma:");
    header("Expires:");
    header("X-Throttled: true");
    __fr_process_download($config["filename"], preg_match("#(\?|&)download(&|$)#", $_SERVER["REQUEST_URI"]), $config["watermark"]);
    if ($__fr_reporting_leech) {
      chdir(__DRUPAL_BASE_DIR);
      @require('includes/bootstrap.inc');
    }
    exit();
  }
}

// unable to send the file, go back into the Drupal system

unset($config);
unset($filepath);
$_SERVER['SCRIPT_NAME'] = dirname(dirname(dirname($_SERVER['SCRIPT_NAME'])))."/index.php";
chdir(__DRUPAL_BASE_DIR);
$_GET["q"] = "system/files";
require("index.php");

?>