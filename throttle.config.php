<?php
$config["filereq_fspath"] = "files";
$config["filereq_antileech_enabled"] = true;
$config["filereq_antileech_regex"] = preg_quote($_SERVER["SERVER_NAME"]);
$config["filereq_antileech_image_mode"] = WATERMARK; // 0 or WATERMARK or SUBSTITUTE
$config["filereq_antileech_image_file"] = "modules/filerequest/watermark.png";
?>