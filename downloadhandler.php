<?php

/*
    Somewhat magic script that will push the server file to the client, taking 
    into account client requests like ranges.

    This is a modified version of the original of charon-dl
    http://sf.net/projects/charon-dl
*/

/*
    Charon - Download System
    Copyright (C) 2005, Michiel "El Muerte" Hendriks

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


define("READBUFFER_SIZE", 4096); // read buffer size
define("USE_GZIP", true); // use GZip compression for text/* files

define("WATERMARK", 1); // add a watermark to the image
define("SUBSTITUTE", 2); // replace the image with an other one

define("UPSAMPLE_PALLET", true); // if set upsample pallet images, produces better results
define("DOWNSAMPLE_TO_ORIGIN", true); // if set to true downsample the watermarked 
                                     // image to the original format, otherwise it will
                                     // be converted to a jpg

/* 
    not OS safe
*/
if (!function_exists("mime_content_type"))  {
  function mime_content_type($f) {
    $f = escapeshellarg($f);
    return trim( exec("file -bi ".$f) );
  }
}

/**
 * Return true if the file is an image file.
 *
 * @param   string      the filename
 * @return  boolean     true if it's an image we want to process
 */
function __fr_is_image_file($filename) {
  // only png, jpg, gif are supported
  return (in_array(StrToLower(preg_replace("#^.*\.([^.]*)$#", "\\1", $filename)), array("jpg", "jpeg", "jpe", "png", "gif")));
}

/**
 * Reports the leech
 */
function __fr_report_leech() {
  global $__fr_reporting_leech;
  $__fr_reporting_leech = true;
  register_shutdown_function("__fr_delayed_report");
}

function __fr_delayed_report() {
  global $__fr_reporting_leech;
  if (!$__fr_reporting_leech) return;
  if (!function_exists('watchdog')) return;
  watchdog('download leech', "File: <i>".$_GET["file"]."</i><br />\nSource: <i>".$_SERVER["HTTP_REFERER"]."</i>", WATCHDOG_NOTICE);
}

/**
 * Detect leeching sites
 *
 * @param   mixed   array containing the configuration
 * @return  bool    false if it's a leech
 */
function __fr_can_download_file(&$config) {
  $config["watermark"] = false;
  if (!$config["filereq_antileech_enabled"]) return true;
  if (!empty($_SERVER["HTTP_REFERER"])) {
    if (!preg_match("#^http(s)?://".$config["filereq_antileech_regex"]."/#i", $_SERVER["HTTP_REFERER"])) {

      if (!isset($config["filereq_nowatchdog"])) __fr_report_leech();

      if (__fr_is_image_file($config["filename"])) {
        switch (intval($config["filereq_antileech_image_mode"])) {
          case WATERMARK:
            $config["watermark"] = __DRUPAL_BASE_DIR.$config["filereq_antileech_image_file"];
            return true;
          case SUBSTITUTE:
            $config["filename"] = __DRUPAL_BASE_DIR.$config["filereq_antileech_image_file"];
            return true;
        }
      }

      return false;
    }
  }
  // empty referer is good, for now
  return true;
}

/** 
 * Send a file to the client. Absolute file path required!
 * @param   bool     forces the file to be send as an attachment (no inline viewing)
 * @param   function is a function in the form: function($filename) and returns the mimetype
 * @return  bool     true if the download was handled
 */
function __fr_process_download($filename, $forcedl=false, $watermark=false, $mimeOverride=null)
{
  global $__fr_reporting_leech;

  if (!file_exists($filename)) {
    trigger_error("Requested download file does not exist: <i>".$filename."</i>", E_USER_ERROR);
    return false;
  }
  if (!is_readable($filename)) {
    trigger_error("Requested file is not readable: <i>".$filename."</i>", E_USER_ERROR);
    return false;
  }

  $fi["path"] = $filename;
  $fi["name"] = basename($filename); // name reported to the browser
  $fi["size"] = filesize($filename);
  $fi["time"] = filemtime($filename);
  $fi["ranges"] = array();

  if ($forcedl) $contentDisposition = "attachment";  // inline/attachment
  else {
    //$_SERVER["HTTP_ACCEPT"];
    $contentDisposition = "inline"; // TODO: improve?
  }

  $do_watermark = false;
  if (file_exists($watermark) && __fr_is_image_file($fi["name"])) {
    $do_watermark = true;
    $fi["name"] = preg_replace("#^(.*)(\.[^.]*)$#", "\\1_branded\\2", $fi["name"]);
  }

  header("Content-Type: "); // unset content type

  if (!$do_watermark && isset($_SERVER["HTTP_RANGE"])) {
    // process ranges
    if (preg_match("/^bytes=(.*)$/", trim($_SERVER["HTTP_RANGE"]), $ranges)) {
      $ranges = explode(",", $ranges[1]);
      for ($i = 0; $i < count($ranges); $i++) {
        if (preg_match("/^([0-9]*)-([0-9]*)$/", $ranges[$i], $r)) {
          if ($r[1] == "") { // -X : last X bytes
            $fi["ranges"][] = array("start" => $fi["size"]-intval($r[2]), "stop" => $fi["size"]-1, "count" => intval($r[2]));
          }
          else if ($r[2] == "") { // X- : skip first X bytes
            $fi["ranges"][] = array("start" => intval($r[1]), "stop" => $fi["size"]-1, "count" => $fi["size"]-intval($r[1]));
          }
          else if ($r[1] == "" && $r[2] == "") {
            $fi["ranges"] = array();
            break;
          }
          else { // X-Y : X to Y bytes (0 = 1st byte)
            $fi["ranges"][] = array("start" => intval($r[1]), "stop" => intval($r[2]), "count" => intval($r[2])-intval($r[1])+1);
          }
        }
        else {
          $fi["ranges"] = array();
          break;
        }
      }
    }

    for ($i = 0; $i < count($fi["ranges"]); $i++) {
      if (($fi["ranges"][$i]["start"] < 0)
        || ($fi["ranges"][$i]["stop"] >= $fi["size"])) {
        $fi["ranges"] = array();
        break;
      }
    }

    if (count($fi["ranges"]) == 0) { // no valid ranges specified
      header("HTTP/1.1 416 Requested range not satisfiable");
      header("Content-Range: bytes */".$fi["size"]);
      return true;
    }
  }
  
  if ($_SERVER["HTTP_IF_MODIFIED_SINCE"]) {
    if ($fi["time"] > strftime($_SERVER["HTTP_IF_MODIFIED_SINCE"])) {
      header("HTTP/1.1 304 Not Modified");
      $__fr_reporting_leech = false; // disable watchdog reporting
      return true;
    }
  }

  // figure out mime last
  $fi["mime"] = "";
  if (is_callable($mimeOverride)) $fi["mime"] = $mimeOverride(basename($filename));
  if (empty($fi["mime"])) {
    if (function_exists("mime_content_type")) $fi["mime"] = mime_content_type($filename);
  }
  if (!preg_match("#^(\w*)/(\w*)$#i", $fi["mime"])) $fi["mime"] = "application/octet-stream";

  // send headers

  if (count($fi["ranges"]) > 0)  {
    header("HTTP/1.1 206 Partial content");
    if (count($fi["ranges"]) == 1) {
      header("Content-Range: bytes ".$fi["ranges"][0]["start"]."-".$fi["ranges"][0]["stop"]."/".$fi["size"]);
      header("Content-Length: ".$fi["ranges"][0]["count"]);
    }
    else {
      $byteRangeBoundary = "_BYTE_RANGE_".md5(microtime());
      header("Content-Type: multipart/byteranges; boundary=".$byteRangeBoundary);
    }
  }
  else {
    header("HTTP/1.1 200 Ok");
    if (!$do_watermark) header("Content-Length: ".$fi["size"]); // we don't know the final size when watermarking
  }

  if (!$do_watermark) header("Accept-Ranges: bytes");
  if (count($fi["ranges"]) <= 1) header("Content-Type: ".$fi["mime"]);
  /*if (!$do_watermark)*/ header("Last-Modified: ".gmdate("D, d M Y H:i:s \G\M\T", $fi["time"]));   
  header("Content-Disposition: ".$contentDisposition."; filename=\"".$fi["name"]."\"");

  // encode text files when accepted
  if (USE_GZIP && preg_match("#^text/#i", $fi["mime"]) && extension_loaded("zlib")
    && in_array("gzip", explode(",", $_SERVER["HTTP_ACCEPT_ENCODING"]))) {
    ob_start("ob_gzhandler");
    ob_implicit_flush(false); // content length header needs to be updated
  }

  if ($_SERVER["REQUEST_METHOD"] == "HEAD") return true; // no body for you

  if ($do_watermark) {
    header("Expires: ".gmdate("D, d M Y H:i:s \G\M\T", mktime()+900)); // expire watermark in 15 minutes
    header("X-Watermarked: true");
    return __fr_add_watermark($filename, $watermark);
  }

  $fp = fopen($filename, "rb");
  if (!$fp) {
    header("HTTP/1.1 500 Internal Server Error");
    echo "Unable to open file for reading.";
    return true;
  }

  if (count($fi["ranges"]) == 0)  {
    while (!connection_status() && !feof($fp)) {
      echo fread($fp, READBUFFER_SIZE);
    }
  }
  else if (count($fi["ranges"]) == 1)  {
    fseek($fp, $fi["ranges"][0]["start"], SEEK_SET);
    while (!connection_status() && (ftell($fp) <= $fi["ranges"][0]["stop"]-READBUFFER_SIZE)) {
      echo fread($fp, READBUFFER_SIZE);
    }
    if ($fi["ranges"][0]["stop"]-ftell($fp) > 1) echo fread($fp, $fi["ranges"][0]["stop"]-ftell($fp)+1);
  }
  else {
    for ($i = 0; $i < count($fi["ranges"]); $i++) {
      echo "\r\n";
      echo "--".$byteRangeBoundary."\r\n";
      echo "Content-Type: ".$fi["mime"]."\r\n";
      echo "Content-Range: bytes ".$fi["ranges"][$i]["start"]."-".$fi["ranges"][$i]["stop"]."/".$fi["size"]."\r\n";
      echo "Content-Disposition: ".$contentDisposition."; filename=".$fi["name"]."\r\n";
      echo "\r\n";

      fseek($fp, $fi["ranges"][$i]["start"], SEEK_SET);
      while (!connection_status() && (ftell($fp) <= $fi["ranges"][$i]["stop"]-READBUFFER_SIZE)) {
        echo fread($fp, READBUFFER_SIZE);
      }
      if ($fi["ranges"][$i]["stop"]-ftell($fp) > 1) echo fread($fp, $fi["ranges"][$i]["stop"]-ftell($fp)+1);
    }
    echo "\r\n";
    echo "--".$byteRangeBoundary."--";
  }

  fclose($fp);
  return true;
}


function __fr_imagetruecolortopalette_ex( $image, $dither, $ncolors )
{
  if (function_exists('imagecolormatch')) {
    $width = imagesx( $image );
    $height = imagesy( $image );
    $colors_handle = ImageCreateTrueColor( $width, $height );
    ImageCopyMerge( $colors_handle, $image, 0, 0, 0, 0, $width, $height, 100 );
  }
  ImageTrueColorToPalette( $image, $dither, $ncolors );
  if (function_exists('imagecolormatch')) {
    ImageColorMatch( $colors_handle, $image );
    ImageDestroy( $colors_handle );
  }
}

/*
  Source of this routine:
  http://www.php.net/manual/en/function.imagecopymerge.php

  slightly modified
*/

function __fr_add_watermark($sourcefile, $watermarkfile) {
   #
   # $sourcefile = Filename of the picture to be watermarked.
   # $watermarkfile = Filename of the 24-bit PNG watermark file.
   #
  
   //Get the resource ids of the pictures
   $watermarkfile_id = imagecreatefrompng($watermarkfile);
  
   imageAlphaBlending($watermarkfile_id, false);
   imageSaveAlpha($watermarkfile_id, true);

   $fileType = getimagesize($sourcefile);

   switch($fileType[2]) {
     case 1:
       $sourcefile_id = imagecreatefromgif($sourcefile);
       break;

     case 2:
       $sourcefile_id = imagecreatefromjpeg($sourcefile);
       break;

     case 3:
       $sourcefile_id = imagecreatefrompng($sourcefile);
       break;

   }

   //Get the sizes of both pix 
  $sourcefile_width=imageSX($sourcefile_id);
  $sourcefile_height=imageSY($sourcefile_id);
  $watermarkfile_width=imageSX($watermarkfile_id);
  $watermarkfile_height=imageSY($watermarkfile_id);

  $dest_x = ( $sourcefile_width / 2 ) - ( $watermarkfile_width / 2 );
  $dest_y = ( $sourcefile_height / 2 ) - ( $watermarkfile_height / 2 );
  
  $upsampled = 0;

  // if a gif, we have to upsample it to a truecolor image
  if(UPSAMPLE_PALLET && !imageistruecolor($sourcefile_id)) {
    // create an empty truecolor container
    $tempimage = imagecreatetruecolor($sourcefile_width, $sourcefile_height);

    $upsampled = imagecolorstotal($sourcefile_id);

    // copy the 8-bit gif into the truecolor image
    imagecopy($tempimage, $sourcefile_id, 0, 0, 0, 0, $sourcefile_width, $sourcefile_height);
    imagedestroy($sourcefile_id);

    // copy the source_id int
    $sourcefile_id = $tempimage;
  }

  imagecopy($sourcefile_id, $watermarkfile_id, $dest_x, $dest_y, 0, 0, $watermarkfile_width, $watermarkfile_height);

  if ($upsampled) {
    if (DOWNSAMPLE_TO_ORIGIN && function_exists('imagetruecolortopalette')) {
      __fr_imagetruecolortopalette_ex($sourcefile_id, true, 256);
    }
    else {
      $fileType[2] = 2; // force to JPEG to reduce size
    }
  }

  //Create a jpeg out of the modified picture
  switch($fileType[2]) {
  
    // remember we don't need gif any more, so we use only png or jpeg.
    // See the upsaple code immediately above to see how we handle gifs
    case 3:
      header("Content-type: image/png");
      imagepng ($sourcefile_id);
      break;

    case 1:
      if (imagetypes() & IMG_GIF) {
        header("Content-type: image/gif");
        imagegif ($sourcefile_id);
        break;
      }

    default:
      header("Content-type: image/jpg");
      imagejpeg ($sourcefile_id);
  }
 
  imagedestroy($sourcefile_id);
  imagedestroy($watermarkfile_id);
}
